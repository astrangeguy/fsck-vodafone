#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>

#include "mystring.h"
#include "connect.h"
#include "linux-x64-syscalls.h"
#include "environ.h"
#include "format.h"

#define DELAY_IN_SECS 30

int main(int argc, cstring argv[]) {

    if (argc != 2) { // print usage
        cstring progname = mybasename(argv[0]);
        static char const msg_useage_1[] = "Usage: "; // <exename>
        static char const msg_useage_2[] = " <URL>\n\tlogs output to the VODAFONE_LOG variable or to vodafone.log\n";
        mywrite(STDERR_FILENO, msg_useage_1, sizeof(msg_useage_1)-1);
        mywrite(STDERR_FILENO, progname, stringlen(progname));
        mywrite(STDERR_FILENO, msg_useage_2, sizeof(msg_useage_2)-1);
        myexit(0);
    }

    cstring url = argv[1];

    char const * const logpath = getenv_default("VODAFONE_LOG", "vodafone.log");

    int logfd = myopen(logpath, O_WRONLY | O_APPEND | O_CREAT, 0664);
    if (logfd < 0) {
        static char const msg_logfile_1[] = "cannot open logfile '";
        static char const msg_logfile_2[] = "' logging to stdout instead\n";
        mywrite(STDERR_FILENO, msg_logfile_1, sizeof(msg_logfile_1)-1);
        mywrite(STDERR_FILENO, logpath, stringlen(logpath));
        mywrite(STDERR_FILENO, msg_logfile_2, sizeof(msg_logfile_2)-1);
        logfd = STDOUT_FILENO;
    }

    for (;; msleep(DELAY_IN_SECS * 1000)) {
        int connect_fd = open_http_get(url);

        { // print date & time
            struct timeval tv;
            mygettimeofday(&tv, NULL);

            char datebuf[64];
            int datelen = format_unixtime(tv.tv_sec, datebuf, sizeof(datebuf));
            mywrite(logfd, datebuf, datelen);
            mywrite(logfd, ": ", 2);
            mywrite(logfd, url, stringlen(url));
        }

        if (connect_fd < 0) {
            static char const msg_unreach[] = " -> host not reachable\n";
            mywrite(logfd, msg_unreach, sizeof(msg_unreach)-1);
            continue;
        }

        // we've had a successful connection

        char http_resp[64];
        char * resp = http_resp;
        int bytes_read = myread(connect_fd, http_resp, sizeof(http_resp));
        http_resp[63] = '\0';

        if (bytes_read <= 0) {
            switch (-bytes_read) {
            //case EWOULDBLOCK:
            case EAGAIN: {
                static char const msg_noresp[] = " -> host reachable, no response\n";
                mywrite(logfd, msg_noresp, sizeof(msg_noresp)-1);
                continue;
            }
            case 0: {
                static char const msg_respemty[] = " -> host reachable, empty response\n";
                mywrite(logfd, msg_respemty, sizeof(msg_respemty)-1);
                continue;
            }
            default: {
                static char const msg_rcverr[] = " -> host reachable, unexpected rcv error\n";
                mywrite(logfd, msg_rcverr, sizeof(msg_rcverr)-1);
                continue;
            }
            }
        }

        // we have a response from the server

        // HTTP/1.X DDD <RESPONSE CODE>\r\n
        resp += sizeof("HTTP/1.1 ")-1;
        char * after_newline = (char *)find_if_substr(resp, "\r\n");

        if (!after_newline) {
            static char const msg_noresp[] = " -> host reachable, malformed response\n";
            mywrite(logfd, msg_noresp, sizeof(msg_noresp)-1);
            continue;
        }

        resp[(after_newline - resp) - 2] = '\0';
        static char const msg_resp_1[] = " -> host reachable (";
        static char const msg_resp_2[] = ")\n";

        mywrite(logfd, msg_resp_1, sizeof(msg_resp_1)-1);
        mywrite(logfd, resp, stringlen(resp));
        mywrite(logfd, msg_resp_2, sizeof(msg_resp_2)-1);

        myfsync(logfd);

        myclose(connect_fd);
    }

}
