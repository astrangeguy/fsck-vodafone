#ifndef FORMAT_H_
#define FORMAT_H_

#include <stddef.h>

/**
 * Prints `unixtime` (no. of seconds from unix epoch) in a human readable (DD-MMM-YYYY h:m:s)
 * format to `buf`.
 * Returns the number of characters printed.
 */
extern int format_unixtime(int unixtime, char * buf, size_t buflen);


#endif /* FORMAT_H_ */
