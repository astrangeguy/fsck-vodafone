#include "linux-x64-syscalls.h"

#include <sys/syscall.h>
#include <unistd.h>
#include <errno.h>

static long _mywrite(int fd, void const * buf, size_t cnt) {
    register long resword asm("rax");
    register long _3 asm("rdx") = (long) cnt;
    register long _2 asm("rsi") = (long) buf;
    register long _1 asm("rdi") = (long) fd;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_write), "r" (_1), "r" (_2), "r" (_3) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}

long mywrite(int fd, void const * buf, size_t cnt) {
    size_t rest_cnt = cnt;

    while (rest_cnt > 0) {
        long written = _mywrite(fd, buf, rest_cnt);
        if (written < 0) {
            if (-written == EINTR)
                continue;
            else
                return written;
        }

        buf += written;
        rest_cnt -= written;
    }

    return cnt;
}

static long _myread(int fd, void * buf, size_t cnt) {
    register long resword asm("rax");
    register long _3 asm("rdx") = (long) cnt;
    register long _2 asm("rsi") = (long) buf;
    register long _1 asm("rdi") = (long) fd;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_read), "r" (_1), "r" (_2), "r" (_3) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}

long myread(int fd, void * buf, size_t cnt) {
    size_t rest_cnt = cnt;

    long hasread;
    do {
        hasread = _myread(fd, buf, rest_cnt);
        if (hasread < 0) {
            if (-hasread == EINTR)
                continue;
            else
                return hasread;
        }

        buf += hasread;
        rest_cnt -= hasread;
    } while (hasread > 0 && rest_cnt > 0);

    return cnt - rest_cnt;
}


long myopen(char const * path, int flags, mode_t mode) {
    register long resword asm("rax");
    register long _3 asm("rdx") = (long) mode;
    register long _2 asm("rsi") = (long) flags;
    register long _1 asm("rdi") = (long) path;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_open), "r" (_1), "r" (_2), "r" (_3) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}

long mynanosleep(struct timespec const * request, struct timespec * remain) {
    register long resword asm("rax");
    register long _2 asm("rsi") = (long) remain;
    register long _1 asm("rdi") = (long) request;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_nanosleep), "r" (_1), "r" (_2) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}



long mygettimeofday(struct timeval * timeval, void * ign_timezone) {
    register long resword asm("rax");
    register long _2 asm("rsi") = (long) ign_timezone;
    register long _1 asm("rdi") = (long) timeval;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_gettimeofday), "r" (_1), "r" (_2) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}


long myexit(int status) {
    register long resword asm("rax");
    register long _1 asm("rdi") = (long) status;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_exit), "r" (_1) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}

long myfsync(int fd) {
    register long resword asm("rax");
    register long _1 asm("rdi") = (long) fd;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_fsync), "r" (_1) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}

long myclose(int fd) {
    register long resword asm("rax");
    register long _1 asm("rdi") = (long) fd;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_close), "r" (_1) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}

long mysocket(int domain, int type, int proto) {
    register long resword asm("rax");
    register long _3 asm("rdx") = (long) proto;
    register long _2 asm("rsi") = (long) type;
    register long _1 asm("rdi") = (long) domain;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_socket), "r" (_1), "r" (_2), "r" (_3) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}

long myconnect(int fd, struct sockaddr * target_addr, int addrlen) {
    register long resword asm("rax");
    register long _3 asm("rdx") = (long) addrlen;
    register long _2 asm("rsi") = (long) target_addr;
    register long _1 asm("rdi") = (long) fd;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_connect), "r" (_1), "r" (_2), "r" (_3) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}

long mysetsockopt(int fd, int lvl, int opt, void const * optval, size_t optlen) {
    register long resword asm("rax");
    register long _5 asm("r8") = (long) optlen;
    register long _4 asm("r10") = (long) optval;
    register long _3 asm("rdx") = (long) opt;
    register long _2 asm("rsi") = (long) lvl;
    register long _1 asm("rdi") = (long) fd;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_setsockopt), "r" (_1), "r" (_2), "r" (_3), "r" (_4), "r" (_5) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}

long mysendto(int fd, void const * buf, size_t buflen, int flags, struct sockaddr const * addr, size_t addrlen) {
    register long resword asm("rax");
    register long _6 asm("r9") = (long) addrlen;
    register long _5 asm("r8") = (long) addr;
    register long _4 asm("r10") = (long) flags;
    register long _3 asm("rdx") = (long) buflen;
    register long _2 asm("rsi") = (long) buf;
    register long _1 asm("rdi") = (long) fd;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_sendto), "r" (_1), "r" (_2), "r" (_3), "r" (_4), "r" (_5), "r" (_6) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}

long myrecvfrom(int fd, void * buf, size_t buflen, int flags, struct sockaddr const * addr, size_t addrlen) {
    register long resword asm("rax");
    register long _6 asm("r9") = (long) addrlen;
    register long _5 asm("r8") = (long) addr;
    register long _4 asm("r10") = (long) flags;
    register long _3 asm("rdx") = (long) buflen;
    register long _2 asm("rsi") = (long) buf;
    register long _1 asm("rdi") = (long) fd;

    asm volatile (
            "syscall" :
            "=a" (resword) :
            "0" (SYS_recvfrom), "r" (_1), "r" (_2), "r" (_3), "r" (_4), "r" (_5), "r" (_6) :
            "memory", "cc", "r11", "cx"
    );

    return resword;
}

void panic(char const * msg) {
    if (msg) {
        char const * last = msg;
        while (*last)
            last++;
        mywrite(STDERR_FILENO, msg, last - msg);
        mywrite(STDERR_FILENO, "\n", 1);
    }
    myexit(42);
}

bool msleep(long ms) {
    struct timespec const ts = (struct timespec) {
        .tv_sec = ms / 1000,
        .tv_nsec = (ms * 1000 * 1000) % (1000L * 1000 * 1000)
    };
    struct timespec ign;
    long res = mynanosleep(&ts, &ign);
    return res >= 0;
}

