#include "format.h"

#include "mystring.h"

/**
 * Prints integer `n` in decimal format to `buf`.
 * Returns the number or characters printed.
 */
static int format_dec(int n, char * buf, size_t buflen) {
    size_t const  orig_buflen = buflen;

    if (!buf)
        return 0;

    if (buflen && n < 0) {
        *buf++ = '-';
        buflen--;
        n = -n;
    }

    char tmp[10];
    int digit = 0;

    while (n > 0) {
        tmp[digit++] = n % 10 + '0';
        n /= 10;
    }

    while (--digit >= 0 && buflen-- > 0) {
        *buf++ = tmp[digit];
    }

    return orig_buflen - buflen;
}

int format_unixtime(int unixtime, char * buf, size_t buflen) {

    int z = unixtime / 86400;
    int sec_in_day = unixtime % 86400;
    int hh = sec_in_day / (86400 / 24);
    int mm = sec_in_day % (60 * 60) / 60;
    int ss = sec_in_day % 60;

    // adapted from
    // http://howardhinnant.github.io/date_algorithms.html#civil_from_days

    z += 719468;
    const int era = (z >= 0 ? z : z - 146096) / 146097;
    const unsigned doe = (unsigned)(z - era * 146097);          // [0, 146096]
    const unsigned yoe = (doe - doe/1460 + doe/36524 - doe/146096) / 365;  // [0, 399]
    int y = ((int)yoe) + era * 400;
    const unsigned doy = doe - (365*yoe + yoe/4 - yoe/100);                // [0, 365]
    const unsigned mp = (5*doy + 2)/153;                                   // [0, 11]
    const unsigned d = doy - (153*mp+2)/5 + 1;                             // [1, 31]
    const unsigned m = mp + (mp < 10 ? 3 : -9);                            // [1, 12]
    y += (m <= 2);


    static char const * const months[] = {
            "OFFSET", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
    };


    // DD-MMM-YYYY hh:mm:ss
    int written = 0;
    written += format_dec(d, buf, buflen);
    written += mystrncpy(buf + written, "-", buflen - written);
    written += mystrncpy(buf + written, months[m], buflen - written);
    written += mystrncpy(buf + written, "-", buflen - written);
    written += format_dec(y, buf + written, buflen - written);
    written += mystrncpy(buf + written, " ", buflen - written);
    written += format_dec(hh, buf + written, buflen - written);
    written += mystrncpy(buf + written, ":", buflen - written);
    written += format_dec(mm, buf + written, buflen - written);
    written += mystrncpy(buf + written, ":", buflen - written);
    written += format_dec(ss, buf + written, buflen - written);

    return written;
}
