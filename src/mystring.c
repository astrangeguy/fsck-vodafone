#include "mystring.h"

size_t stringlen(cstring str) {
    size_t ret = 0;
    while(*str++)
        ret++;
    return ret;
}

cstring mybasename(cstring path) {
    cstring after_slash = path;
    while ((after_slash = find_if_substr(path, "/"))) {
        path = after_slash;
    }
    return path;
}

cstring skip_leading_ws(cstring str) {
    if (!str)
        return NULL;

    while (*str == ' ' || *str == '\t' || *str == '\n') {
        str++;
    }
    return str;
}

cstring find_if_prefix(cstring str, cstring prefix) {
    while (*prefix) {
        if (*prefix++ != *str++)
            return NULL;
    }
    return str;
}

cstring find_if_substr(cstring str, cstring substr) {
    while (*str) {
        cstring ptrstr = str;
        cstring ptrsubstr = substr;

        while (*ptrstr && *ptrsubstr && *ptrstr == *ptrsubstr) {
            ptrstr++;
            ptrsubstr++;
        }

        if (*ptrsubstr == '\0') {
            return ptrstr;
        } else {
            str++;
        }
    }
    return NULL;
}

int mystrncpy(char * const dest, cstring src, size_t count) {
    if (!dest)
        return 0;

    char * dest_write = dest;
    int ret = 0;

    while (*src && count-- > 0) {
        *dest_write++ = *src++;
        ret++;
    }
    *dest_write = '\0';

    return ret;
}


