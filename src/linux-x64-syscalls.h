/**
 * A subset of POSIX syscalls required to write urlcheck.
 * Implemented without `errno` functionality.
 * Return values are of type `long` instead of int and classical errno is equal to `-retval` if negative.
 */
#ifndef LINUX_X64_SYSCALLS_H_
#define LINUX_X64_SYSCALLS_H_

#include <stddef.h>
#include <time.h>
#include <sys/socket.h>
#include <stdbool.h>

extern long mywrite(int fd, void const * buf, size_t cnt);
extern long myread(int fd, void * buf, size_t cnt);
// myread and mywrite both try to resume when they have not read the `cnt` amount or were interrupted

extern long myopen(char const * path, int flags, mode_t mode);
extern long myexit(int status);
extern long myfsync(int fd);
extern long myclose(int fd);
extern long mynanosleep(struct timespec const * request, struct timespec * remain);
extern long mygettimeofday(struct timeval * tv, void * ign_timezone);
extern long mysocket(int domain, int type, int proto);
extern long myconnect(int fd, struct sockaddr * target_addr, int addrlen);
extern long mysetsockopt(int fd, int lvl, int opt, void const * optval, int optlen);
extern long mysendto(int fd, void const * buf, size_t buflen, int flags, struct sockaddr const * addr, size_t addrlen);
extern long myrecvfrom(int fd, void * buf, size_t buflen, int flags, struct sockaddr const * addr, size_t addrlen);

/**
 * Prints `msg` to stderr and exits with -1.
 */
void panic(char const * msg);

/**
 * Sleeps for `ms` milliseconds. Returns false when interrupted by a signal.
 */
extern bool msleep(long ms);

#endif /* LINUX_X64_SYSCALLS_H_ */
