#ifndef ENVIRON_H_
#define ENVIRON_H_

/**
 * Return the value of the `var` environment variable or `fallback` if the variable is not defined.
 * (getting the value of a defined but empty variable returns the empty string)
 */
extern char const * getenv_default(char const * varname, char const * fallback);


#endif /* ENVIRON_H_ */
