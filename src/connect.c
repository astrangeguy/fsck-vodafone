#include "connect.h"

#include <time.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>

#include "linux-x64-syscalls.h"
#include "mystring.h"

static bool setsocktimeout_ms(int fd, int timeout_ms) {
    struct timeval const tv = (struct timeval) {
       .tv_sec = timeout_ms / 1000,
       .tv_usec = (timeout_ms % 1000) * 1000,
    };
    bool res = true;

    res &= mysetsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) >= 0;
    res &= mysetsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) >= 0;

    return res;
}

/**
 * Translates a `hostname` string to a sockaddr that can be used in the `connect` syscall.
 * Returns false when DNS resolution fails.
 *
 * This is the functionality that ruins '-nostdlib' since DNS lookup is actually more complex
 * than the rest of the assignment.
 * Incidentally this also forbids '-nostartfiles' since `getaddrinfo` allocates memory and thus the
 * C library needs to initialize the malloc-machinery.
 */
static bool resolve_host(char const * hostname, struct sockaddr_in * out_sockaddr) {

    struct addrinfo * addrinfo_res = NULL;

    static struct addrinfo const ipv4_hint = ((struct addrinfo){ .ai_family = AF_INET, .ai_socktype = SOCK_STREAM });

    int gai_res = getaddrinfo(hostname, "80", &ipv4_hint, &addrinfo_res);

    if (gai_res < 0) {
        switch (gai_res) {
        case EAI_FAIL:
        case EAI_AGAIN:
        case EAI_NONAME: {
            static char const msg_resolve[] = "Could not resolve host: ";
            mywrite(STDERR_FILENO, msg_resolve, sizeof(msg_resolve)-1);
            mywrite(STDERR_FILENO, hostname, stringlen(hostname));
            mywrite(STDERR_FILENO, "\n", 1);
            return false;
        }
        default:
            panic("unexpected error from calling getaddrinfo");
        }
    }

    if (addrinfo_res && out_sockaddr) {
        *out_sockaddr = *((struct sockaddr_in *)addrinfo_res->ai_addr);
    }

    freeaddrinfo(addrinfo_res);
    return true;
}

extern int open_http_get(char const * url) {

    url = skip_leading_ws(url);

    {
        char const * skipped_http_protocol = find_if_prefix(url, "http://");

        if (skipped_http_protocol)
            url = skipped_http_protocol;
    }

    char hostpart[HOST_MAXLEN];

    char const * pathpart = find_if_substr(url, "/");
    if (pathpart) {
        pathpart--;
        mystrncpy(hostpart, url, pathpart - url);
    } else {
        mystrncpy(hostpart, url, stringlen(url));
        pathpart = "/";
    }

    // fail if url contained an unknown protocol
    {
        cstring after_proto_in_host = find_if_substr(hostpart, "://");
        if (after_proto_in_host) {
            hostpart[(after_proto_in_host - hostpart) - 3] = '\0';

            static char const msg_unsupp_proto[] = "Unsupported protocol: ";
            mywrite(STDERR_FILENO, msg_unsupp_proto, sizeof(msg_unsupp_proto)-1);
            panic(hostpart);
        }
    }

    struct sockaddr_in addr;
    if (!resolve_host(hostpart, &addr))
        return -EADDRNOTAVAIL;

    long socketfd = mysocket(AF_INET, SOCK_STREAM, 0);
    if (socketfd < 0)
        return socketfd;

    if (!setsocktimeout_ms(socketfd, 1000)) {
        myclose(socketfd);
        return -ENOTSUP;
    }

    {
        long connect_res = myconnect(socketfd, (struct sockaddr *) &addr, sizeof(addr));
        if (connect_res < 0) {
            myclose(socketfd);

            switch (-connect_res) {
            case ECONNREFUSED:
            case ENETUNREACH:
                return -ENETUNREACH;
            default:
                panic("unexpected error from calling connect");
            }
        }
    }

    {
        // GET <path> HTTP/1.1
        // Host: <host>
        //
        static char const p_GET[] = "GET ";
        static char const p_HTTP11[] = " HTTP/1.1\r\n";
        static char const p_Host[] = "Host: ";
        static char const p_NLNL[] = "\r\n\r\n";

        int res;
        res = mywrite(socketfd, p_GET, sizeof(p_GET)-1);
        if (res < 0)
            goto check_err;
        res = mywrite(socketfd, pathpart, stringlen(pathpart));
        if (res < 0)
            goto check_err;
        res = mywrite(socketfd, p_HTTP11, sizeof(p_HTTP11)-1);
        if (res < 0)
            goto check_err;
        res = mywrite(socketfd, p_Host, sizeof(p_Host)-1);
        if (res < 0)
            goto check_err;
        res = mywrite(socketfd, hostpart, stringlen(hostpart));
        if (res < 0)
            goto check_err;
        res = mywrite(socketfd, p_NLNL, sizeof(p_NLNL)-1);
            goto check_err;

    check_err:
        if (res < 0) {
            switch (-EADDRNOTAVAIL) {
            case EAGAIN:
            //case EWOULDBLOCK:
            case EIO:
                myclose(socketfd);
                return -EADDRNOTAVAIL; // not responding for at least one second
            default:
                panic("unexpected error from sending HTTP request");
            }
        }
    }

    return socketfd;
}
