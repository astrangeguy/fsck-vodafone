#ifndef CONNECT_H_
#define CONNECT_H_

#include <stdbool.h>

#define HOST_MAXLEN 256

/**
 * Opens a socket and connects to `url` to do a HTTP GET request.
 * Returns a file descriptor for the socket, or a negative value that can be interpreted
 * as an negated `errno` value.
 * If lookup fails then -EADDRNOTAVAIL is returned.
 */
extern int open_http_get(char const * url);


#endif /* CONNECT_H_ */
