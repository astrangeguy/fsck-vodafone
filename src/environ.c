#include "environ.h"

#include "mystring.h"

/**
 * Return the value of the `var` environment variable or `fallback` if the variable is not defined.
 * (getting the value of a defined but empty variable returns the empty string)
 */
cstring getenv_default(cstring varname, cstring fallback) {

    /**
     * Locally forward declare the global environment pointer (as defined by POSIX).
     * (else we would have to fetch it manually from argv[argc+1] in `_start`)
     */
    extern char const * const * const environ;

    cstring const * curr_env_ptr = environ;
    while (*curr_env_ptr) {
        cstring found_var = find_if_prefix(*curr_env_ptr++, varname);
        if (found_var)
            return *found_var == '=' ? ++found_var : found_var; // skip the '=' in environment variables
    }
    return fallback;
}
