#ifndef MYSTRING_H_
#define MYSTRING_H_

#include <stddef.h>
#include <stdbool.h>

typedef char const * cstring;

/**
 * Returns the length of a null-terminated string.
 */
extern size_t stringlen(cstring str);

/**
 * Returns the filename part of `path`.
 */
extern cstring mybasename(cstring path);

/**
 * Returns a pointer to the first character in `str` that isn't a ' ', '\t' or '\n' or NULL if `str` is NULL.
 */
extern cstring skip_leading_ws(cstring str);

/**
 * Returns a pointer into `str` after `prefix` if `str` actually does start with `prefix`.
 * Returns `NULL` otherwise.
 */
extern cstring find_if_prefix(cstring str, cstring prefix);

/**
 * Returns a pointer into `str` after the first occurrence of `substr` in it, or `NULL` if `str` does not
 * contain `substr`.
 */
extern cstring find_if_substr(cstring str, cstring substr);

/**
 * An implementation of cstdlib `strncopy` that returns the number of chars actually written.
 */
extern int mystrncpy(char * dest, cstring src, size_t count);

#endif /* MYSTRING_H_ */
