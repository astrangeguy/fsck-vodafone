#ifndef LOOKUP_H_
#define LOOKUP_H_

#include <sys/socket.h>
#include <stdbool.h>

extern bool resolve_addr(char const * hostname, struct sockaddr * out_addr);


#endif /* LOOKUP_H_ */
